## Description

This is the front-end repository for the back-end [repo](https://bitbucket.org/calixto_gonzalez/todo-api/src/master/) it is a React App that was created using create-react-app.

## Instalation

First you need to install npm from [Npm](https://www.npmjs.com/get-npm) then clone the repo once that is done do `cd todo-front && npm install && npm run start` **you should have the backend running before you start the application**

## Configuration

If you want to change the port and url with envairomental variables, like so `REACT_APP_URL=someurl npm start` this would change the URL to which the frontend communicates to the api. You can do the same thing with REACT_APP_PORT changing the port.
