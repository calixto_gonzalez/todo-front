import React, {useEffect, useState} from 'react';
import 'typeface-roboto';
import './App.css';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {green} from '@material-ui/core/colors';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircleOutlineRoundedIcon from '@material-ui/icons/CheckCircleOutlineRounded';
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import AddTodo from "./components/AddTodo";
import MaterialTable from 'material-table';
import {isStatusOK} from "./lib/utlis";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Filters from "./components/Filters";
import RefreshIcon from '@material-ui/icons/Refresh';
import IconButton from "@material-ui/core/IconButton";

const URL = process.env.REACT_APP_URL || "http://localhost";
const PORT = process.env.REACT_APP_PORT || 8080;


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    typography: {
        color: "#42a5f5"
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    button: {
        margin: theme.spacing(1),
    }
}));

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function App() {
    const classes = useStyles();
    let [items, setItems] = useState([]);

    const [openError, setOpenError] = useState(false);
    const [open, setOpen] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const fetchData = async () => {
        fetch(`${URL}:${PORT}/todo/`, {method: 'GET'}).then(res => res.json()).then(result => {
            if (result.body) {
                setItems(result.body.todos)
            }
        }).catch(error => {
            setOpenError(true);
            setErrorMessage(error.message || 'There was an error during screen loading')
        })
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
        setOpenError(false)
    };

    const getStateIcon = (rowData) => rowData.state === 'done' ? <CheckCircleOutlineRoundedIcon style={{color: green[500]}}/> : <RadioButtonUncheckedIcon/>;

    const markAsDone = async (event, rowData) => {
        fetch(`${URL}:${PORT}/todo/${rowData._id}`, {method: 'PUT', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({state: 'done'})})
            .then(res => {
                if (!isStatusOK(res.status)) throw new Error(res.message);
                setOpen(true);
                setSuccessMessage(`TODO ID:${rowData._id} was marked as done successfully`);
                setItems(fetchData)
            })
            .catch(error => {
                setOpenError(true);
                setErrorMessage(error.message || 'There was an error')
            })
    }

    const fileDownload = (blob, rowData) => {
        const a = document.createElement("a");
        a.style.display = "none";
        document.body.appendChild(a);
        a.href = window.URL.createObjectURL(blob);

        // Use download attribute to set set desired file name
        a.setAttribute("download", `${rowData._id}_document_${new Date().getTime()}`);

        // Trigger the download by simulating click
        a.click();

        // Cleanup
        window.URL.revokeObjectURL(a.href);
        document.body.removeChild(a);
    };

    const getAttachmentBlob = (event, rowData) => fetch(`${URL}:${PORT}/todo/attachment/${rowData._id}`, {method: 'GET'})
        .then(res => res.blob())
        .then(blob => fileDownload(blob, rowData))
        .then(res => {
            setOpen(true);
            setSuccessMessage('File downloaded successfully')
        })
        .catch(error => {
            setOpenError(true);
            setErrorMessage(error.message || 'There was an error downloading the file')
        });


    const deleteTodo = (event, rowData) => fetch(`${URL}:${PORT}/todo/${rowData._id}`, {method: 'DELETE'})
        .then(res => {
            if (!isStatusOK(res.status)) throw new Error(res.message);
            setOpen(true);
            setSuccessMessage(`TODO ID:${rowData._id} was deleted successfully`);
            setItems(fetchData)
        })
        .catch(error => {
            setOpenError(true);
            setErrorMessage(error.message || 'There was an error during deletion of TODO')
        })


    useEffect(() => {
        fetchData()
    }, []);


    return (
        <div className="App">
            <header className="App-header">

                <Container>
                    <Grid
                        container
                        spacing={3}
                        className={classes.root}
                    >
                        <Grid item xs={12}>
                            <Typography variant={"h3"} gutterBottom className={classes.typography}>TODOs
                                list</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Divider/>
                        </Grid>
                        <Grid item container  justify='flex-start' alignContent="center" xs={6}>
                            <Filters setItems={setItems} setOpenError={setOpenError} setErrorMessage={setErrorMessage} setSuccessMessage={setSuccessMessage} setOpen={setOpen} />
                        </Grid>
                        <Grid item container justify='flex-end' alignContent="center" xs={6}>
                            <IconButton color="primary">
                                <RefreshIcon onClick={fetchData} fontSize="large" />
                            </IconButton>
                        </Grid>
                        <Grid item xs={12}>
                            <AddTodo fetchItems={fetchData}/>
                        </Grid>
                        <Grid item xs={12}>
                            <Divider/>
                        </Grid>
                        <Grid item xs={12} container alignContent="center" justify='space-around'>

                            <MaterialTable style={{width: '100%'}}
                                           columns={[
                                               {title: 'Creation date', field: 'createdAt'},
                                               {title: 'ID', field: '_id'},
                                               {title: 'Description', field: 'description'},
                                               {title: 'State', field: 'state', render: getStateIcon}
                                           ]}

                                           title={"TODOs list"}
                                           data={items}
                                           actions={[

                                               rowData => (rowData.state === 'done' ? '' : {
                                                   icon: 'done',
                                                   tooltip: 'Mark TODO as done',
                                                   onClick: markAsDone
                                               }),
                                               rowData => (rowData.attachment ? {
                                                   icon: 'get_app',
                                                   tooltip: 'Download Attachment',
                                                   onClick: getAttachmentBlob
                                               } : ''),
                                               {
                                                   icon: 'delete',
                                                   tooltip: 'Deletes TODO',
                                                   onClick: deleteTodo,
                                               },
                                           ]}
                                           options={{
                                               actionsColumnIndex: -1
                                           }}
                            >

                            </MaterialTable>
                        </Grid>

                    </Grid>
                </Container>
                <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity={'success'}>
                        {successMessage}
                    </Alert>
                </Snackbar>
                <Snackbar open={openError} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity={'error'}>
                        {errorMessage}
                    </Alert>
                </Snackbar>
            </header>
        </div>
    );
}

export default App;