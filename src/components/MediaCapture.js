import React, {Fragment} from 'react';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
    input: {
        display: 'none'
    },
    button: {
        margin: theme.spacing(1)
    }
}));

function MediaCapture(props) {

    const classes = useStyles();

    return (
        <Fragment>
            <input
                accept="application/msword, application/vnd.ms-excel, text/plain, application/pdf, image/*"
                className={classes.input}
                id="icon-button-photo"
                onChange={props.handleUpload}
                type="file"
            />
                <label htmlFor="icon-button-photo">
                    <Button
                        variant="outlined"
                        component="span"
                        color="primary"
                        className={classes.button}
                        endIcon={<CloudUploadIcon/>}
                    >
                        Upload
                    </Button>
                </label>
        </Fragment>
    );

}

export default MediaCapture;