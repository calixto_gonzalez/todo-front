import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
const allowedStates = ['pending', 'done'];
const URL = process.env.REACT_APP_URL || "http://localhost";
const PORT = process.env.REACT_APP_PORT || 8080;

export default function Filters(props) {
    const [open, setOpen] = useState(false);
    const [state, setState] = useState(undefined);
    const [ID, setID] = useState(undefined);
    const [description, setDescription] = useState(undefined);
    const [IDError, setIDError] = useState(false);
    const [stateError, setStateError] = useState(false);
    const [error, setError] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    function getParams() {

        let params = {
            description : description,
            state : state,
            id: ID
        }
        Object.keys(params).forEach(key => params[key] === undefined && delete params[key]);
        let esc = encodeURIComponent;
        return Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');


    }

    const handleSubmit = (target) => {

        if (!error) {
            let buildParams = getParams()

            setOpen(false);
            fetch(`${URL}:${PORT}/todo?${buildParams}`).then(res => res.json()).then(res => {
                if (res.body) {
                    props.setItems(res.body.todos);
                    props.setSuccessMessage('Filters applied')
                } else {
                    props.setItems([]);
                    props.setSuccessMessage('No records were found')
                }
                props.setOpen(true)
            }).catch(error => {
                props.setOpenError(true);
                props.setErrorMessage(error.message || 'There was an error during screen loading')
            })
            setDescription(undefined);
            setID(undefined);
            setState(undefined);
        }

    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleOnChangeID = (event) => {
        if (event.target.value && !Number(event.target.value)) {
            setID(undefined)
            setIDError(true)
            setError(true)
        } else {
            setID(event.target.value)
            setIDError(false);
            setError(false);
        }
    }
    const handleStateChange = (event) => {
        if (event.target.value && !allowedStates.includes(event.target.value.toLowerCase().trim())) {
            setState(undefined);
            setStateError(true);
            setError(true);
        } else {
            setState(event.target.value);
            setStateError(false);
            setError(false);
        }
    }

    const handleDescriptionChange = (event) => {
        if (event.target.value && event.target.value.trim() !== '')
            setDescription(event.target.value.trim())
    }

    return (
        <div>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Filter TODOs
            </Button>
            <Dialog maxWidth="md" open={open} onClose={handleClose} aria-labelledby="form-dialog-title">

                <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>

                <DialogContent>
                    <DialogContentText>
                        Enter ID
                    </DialogContentText>
                    <TextField
                        error={IDError}
                        onChange={handleOnChangeID}
                        autoFocus
                        margin="dense"
                        helperText='ID should be a number'
                        id="id"
                        label="ID"
                        type="id"
                        fullWidth
                    />
                    <TextField
                        onChange={handleDescriptionChange}
                        margin="dense"
                        id="description"
                        label="description"
                        type="description"
                        fullWidth
                    />
                    <TextField
                        error={stateError}
                        onChange={handleStateChange}
                        helperText="State should be pending OR done"
                        margin="dense"
                        id="state"
                        label="state"
                        type="state"
                        fullWidth
                    />

                </DialogContent>
                <DialogActions>

                    <Button onClick={handleClose} color="primary">
                        Close
                    </Button>
                    <Button onClick={handleSubmit} color="primary">
                        Submit
                    </Button>
                </DialogActions>

            </Dialog>

        </div>
    );
}