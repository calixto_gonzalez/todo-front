import React, {useState} from 'react';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import MediaCapture from "./MediaCapture";
import Button from "@material-ui/core/Button";
import SendIcon from '@material-ui/icons/Send';
import CheckIcon from '@material-ui/icons/Check';
import {green} from "@material-ui/core/colors";
import TextField from "@material-ui/core/TextField";
import {isStatusOK} from "../lib/utlis";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const URL = process.env.REACT_APP_URL || "http://localhost";
const PORT = process.env.REACT_APP_PORT || 8080;

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(0),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    button: {
        margin: theme.spacing(1),
    },
}));

function AddTodo(props) {

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }


    const classes = useStyles();
    const [description, setDescription] = useState('');
    const [file, setFile] = useState(undefined);


    const [openError, setOpenError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const [open, setOpen] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');

    const [disabled, setDisabled] = useState(false);


    const validateDescription = (target) => {
        if (!description) {
            return false;
        } else {
            return true;
        }
    }

    const sendTodo = (target) => {
        const isValid = validateDescription(target);
        if (isValid) {
            let formData = new FormData();
            file ? formData.append('todoAttachment', file) : console.warn('There is not attachment in TODO');
            formData.append('description', description);
            formData.append('state', 'pending');
            setDisabled(true)
            fetch(`${URL}:${PORT}/todo/`, {method: 'POST', body: formData})
                .then(res => {
                    props.fetchItems();
                    if (!isStatusOK(res.status)) throw new Error(res.message);
                    return res.json()
                })
                .then(res => {
                    setSuccessMessage(res.message);
                    setOpen(true);
                })
                .catch(error => {
                        setErrorMessage(error);
                        setOpenError(true);
                        console.error(error)
                    }
                );
        } else {
            setErrorMessage('Description should have a body');
            setOpenError(true);
        }

        setDisabled(false);
        setFile(undefined);

    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
        setOpenError(false)
    };

    const handleTextFieldOnChange = ({target}) => {
        setDescription(target.value)
    };

    const handleCapture = ({target}) => {
        setFile(target.files[0])
    };

    return (<React.Fragment>
            <Paper className={classes.paper}>
                <Grid container spacing={1}>
                    <Grid container item xs={8} alignItems="center">
                        <TextField onChange={handleTextFieldOnChange} style={{margin: 8}} fullWidth id="standard-basic" label="Description"/>
                    </Grid>
                    <Grid container item xs={4} justify="flex-end" alignItems="center">
                        {file ? <CheckIcon style={{color: green[500]}} /> : '' }
                        <MediaCapture handleUpload={handleCapture}/>
                        <Button
                            variant="outlined"
                            color="secondary"
                            disabled={disabled}
                            className={classes.button}
                            endIcon={<SendIcon/>}
                            onClick={sendTodo}
                        >
                            Send
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={'success'}>
                    {successMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={openError} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity={'error'}>
                    {errorMessage}
                </Alert>
            </Snackbar>
        </React.Fragment>
    );
}

export default AddTodo;