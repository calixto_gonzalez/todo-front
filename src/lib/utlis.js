exports.isStatusOK = (status) => status >= 200 && status < 300;
